export class TreeNode {
    constructor(
        public data: any,
        public children: Array<TreeNode>,
        public totalChildrenCount: number,
        public opened: boolean = false,
    ) { }
}