export class DBEntity{
    constructor(
        public type: string,
        public name: string,
        public hasContentAccess: boolean,
    ){}
}