import { TreeNode } from './tree-node';
import { PaginationResponse } from './pagination-response';
import { Observable } from 'rxjs';

export interface TreeApi{
    getRoot(): Observable<TreeNode>,
    getChildren(item: TreeNode, from?: number, size?: number): Observable<PaginationResponse<TreeNode>>,
}
