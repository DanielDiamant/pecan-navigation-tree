export class PaginationResponse<T>{
    constructor(
        public data: T[],
        public total: number,
    ){}
}