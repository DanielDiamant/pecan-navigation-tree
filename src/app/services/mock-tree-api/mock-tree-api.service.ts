import { Injectable } from '@angular/core';
import { TreeApi } from 'src/app/models/tree-api';
import { TreeNode } from 'src/app/models/tree-node';
import { DBEntity } from 'src/app/models/db-entity';
import { PaginationResponse } from 'src/app/models/pagination-response';
import { of, Observable, observable } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import { DbEntitiesService } from '../db-entities/db-entities.service';

const defaultPageSize = 5;
const mockDelayMs = 0;

@Injectable({
  providedIn: 'root',
})
export class MockTreeApiService implements TreeApi {

  constructor(private dbEntitiesService: DbEntitiesService) { }

  getRoot(): Observable<TreeNode> {
    const type = this.dbEntitiesService.getRoot();
    const dbEntity = new DBEntity(type, type, true);
    const treeItem = new TreeNode(dbEntity, [], 30, true);
    return this.getChildren(treeItem).pipe(map(pagination => {
      treeItem.children.push(...pagination.data);
      return treeItem;
    }))
  }

  getChildren(item: TreeNode, from: number = 0, size: number = defaultPageSize): Observable<PaginationResponse<TreeNode>> {
    const data = new Array(size).fill(null).map((_v, i) => this.generateTreeNode(i + from, item));
    const total = 40;
    return of(new PaginationResponse(data, total)).pipe(delay(mockDelayMs))
  }

  generateTreeNode(id: number, parent?: TreeNode): TreeNode{
    const type = parent ? this.dbEntitiesService.getNextEntityTypeInHierarchy(parent.data.type) : 'root';
    const name = type + " " + id;
    const hasAccess = id % 3 != 0;
    const dbEntity = new DBEntity(type, name, hasAccess);
    const willHaveChildren = this.dbEntitiesService.canItemHaveChildren(dbEntity);
    return new TreeNode(dbEntity, willHaveChildren && [], willHaveChildren && 30)
  }

}
