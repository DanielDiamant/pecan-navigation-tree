import { Injectable } from '@angular/core';
import { DBEntity } from 'src/app/models/db-entity';

const entityTypeHierarchy = ["Home", "Connection", "Database", "Schemas", "Tables", "Columns"];

@Injectable({
  providedIn: 'root'
})
export class DbEntitiesService {

  public entityTypeHierarchy = entityTypeHierarchy;
  
  constructor() { }

  getRoot(): string{
    return entityTypeHierarchy[0];
  }

  canItemHaveChildren(item: DBEntity){
    const index = entityTypeHierarchy.indexOf(item.type);
    return index < entityTypeHierarchy.length-1;
  }

  getNextEntityTypeInHierarchy(current: string): string {
    const index = entityTypeHierarchy.indexOf(current);
    if (index < 0)
      return null;
    return entityTypeHierarchy[index + 1];
  }
}
