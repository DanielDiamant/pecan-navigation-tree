import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { UsageModule } from './usage/usage.module';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    UsageModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
