import { Component, OnInit } from '@angular/core';
import { TreeNode } from '../../models/tree-node';
import { MockTreeApiService } from '../../services/mock-tree-api/mock-tree-api.service';

@Component({
  selector: 'app-usage-screen',
  templateUrl: './usage-screen.component.html',
  styleUrls: ['./usage-screen.component.scss'],
  providers: [
    MockTreeApiService
  ]
})
export class UsageScreenComponent implements OnInit {

  constructor(public mockTreeApiService: MockTreeApiService) { }

  ngOnInit(): void {
  }

}
