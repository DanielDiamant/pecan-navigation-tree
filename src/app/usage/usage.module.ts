import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsageScreenComponent } from './usage-screen/usage-screen.component';
import { BaseComponentsModule } from "../base-components/base-components.module";



@NgModule({
  declarations: [UsageScreenComponent],
  imports: [
    CommonModule,
    BaseComponentsModule
  ],
  exports: [
    UsageScreenComponent
  ]
})
export class UsageModule { }
