import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TreeComponent } from './tree/tree.component';
import { CardComponent } from './card/card.component';
import { NavigatorTreeComponent } from './navigator-tree/navigator-tree.component';
import { DbEntityComponent } from './db-entity/db-entity.component';
import { DbNavigatorTreeComponent } from './db-navigator-tree/db-navigator-tree.component';



@NgModule({
  declarations: [TreeComponent, CardComponent, NavigatorTreeComponent, DbEntityComponent, DbNavigatorTreeComponent],
  imports: [
    CommonModule
  ],
  exports: [
    TreeComponent,
    CardComponent,
    NavigatorTreeComponent,
    DbEntityComponent,
    DbNavigatorTreeComponent,
  ]
})
export class BaseComponentsModule { }
