import { Component, OnInit, Input, Output, EventEmitter, ContentChild } from '@angular/core';
import { TreeNode } from 'src/app/models/tree-node';
import { TreeApi } from 'src/app/models/tree-api';
import { TreeManager } from './tree-manager/tree-manager';

@Component({
  selector: 'app-navigator-tree',
  templateUrl: './navigator-tree.component.html',
  styleUrls: ['./navigator-tree.component.scss']
})
export class NavigatorTreeComponent implements OnInit {

  @Input() treeApi: TreeApi;
  @ContentChild("itemTemplate") template;
  @ContentChild("footerTemplate") footerTemplate;
  treeManager: TreeManager
  
  constructor() {
    this.toggleItem = this.toggleItem.bind(this);
  }
  
  ngOnInit(): void {
    this.treeManager = new TreeManager(this.treeApi);
  }

  toggleItem(item: TreeNode) {
    item.opened = !item.opened;
    if (item.opened && item.children && !item.children.length)
      this.treeManager.load(item);

    if (!item.opened)
      this.treeManager.clean(item);
  }

  getRoot(){
    return this.treeManager.root;
  }

  loadMore(item: TreeNode){
    this.treeManager.load(item, item.children.length);
  }

}
