import { TreeNode } from '../../../models/tree-node';
import { TreeApi } from '../../../models/tree-api';
import { Observable } from 'rxjs';

export class TreeManager {
  public root: Observable<TreeNode>

  constructor(private treeApi: TreeApi) {
    this.root = this.treeApi.getRoot();
  }

  load(item: TreeNode, from?: number, size?: number) {
    this.treeApi.getChildren(item, from, size).subscribe(res => {
      item.children.push(...res.data);
    })
  }

  clean(item: TreeNode) {
    item.children = [];
  }
}
