import { Component, OnInit, Output, EventEmitter, Input, ContentChild } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @Output() onClick = new EventEmitter<void>();
  @Input() clickable: boolean = false;
  @Input() header: string;
  @ContentChild("content") content;

  constructor() { }

  ngOnInit(): void {
  }

  click($event){
    this.onClick.emit($event);
  }

}
