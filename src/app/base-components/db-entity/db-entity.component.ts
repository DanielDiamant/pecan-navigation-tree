import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DBEntity } from 'src/app/models/db-entity';
import { DbEntitiesService } from '../../services/db-entities/db-entities.service';

@Component({
  selector: 'app-db-entity',
  templateUrl: './db-entity.component.html',
  styleUrls: ['./db-entity.component.scss'],
  providers: [DbEntitiesService]
})
export class DbEntityComponent implements OnInit {
  @Input() item: DBEntity;
  @Output() onToggle = new EventEmitter();

  constructor(private dbEntitiesService: DbEntitiesService) { }

  ngOnInit(): void {
  }

  isClickable(){
    return this.item.hasContentAccess && this.dbEntitiesService.canItemHaveChildren(this.item)
  }

}
