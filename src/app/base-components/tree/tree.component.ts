import { Component, OnInit, Input, ContentChild } from '@angular/core';
import { TreeNode } from '../../models/tree-node';

@Component({
  selector: 'app-tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.scss']
})
export class TreeComponent implements OnInit {
  @Input() item: TreeNode
  @ContentChild("itemTemplate") template;
  @ContentChild("footerTemplate") footer;

  constructor() {
  }

  ngOnInit(): void {
  }

}
