import { Component, OnInit } from '@angular/core';
import { MockTreeApiService } from 'src/app/services/mock-tree-api/mock-tree-api.service';

@Component({
  selector: 'app-db-navigator-tree',
  templateUrl: './db-navigator-tree.component.html',
  styleUrls: ['./db-navigator-tree.component.scss'],
  providers: [
    MockTreeApiService
  ]
})
export class DbNavigatorTreeComponent implements OnInit {

  constructor(public mockTreeApiService: MockTreeApiService) { }

  ngOnInit(): void {
  }

}
